class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :firstName
      t.string :middleName
      t.string :lastName
      t.date :birthday

      t.timestamps
    end
  end
end
