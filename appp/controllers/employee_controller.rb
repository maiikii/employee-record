class EmployeeController < ApplicationController
  before_action :set_employee, only: [:edit, :update, :destroy]

  def index
    @employees = Employee.all
  end

  def new
    @employee = Employee.new
  end

  def create
    @employee = Employee.new(employee_params)

    if @employee.save
      redirect_to employee_index_path, notice: "The employee has been created!" and return
    end
    render 'new'
  end

  def edit
  end

  def update
    if @employee.update_attributes(employee_params)
      redirect_to employee_index_path, notice: 'The employee has been updated!' and return
    end
    render 'edit'
  end

  def destroy
    @employee.destroy
    redirect_to employee_index_path, notice: 'The employee  has been deleted!' and return
  end

  private
    def set_employee
      @employee = Employee.find(params[:id])
    end

    def employee_params
      params.require(:employee).permit(:first_name, :middle_name, :last_name, :birthday,
                                       :job_title_id, :department_id, :start_date, :end_date)
    end
end
