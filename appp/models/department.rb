class Department
  include Mongoid::Document
  field :name, type: String
  
  belongs_to :employee, foreign_key: :deptartment_id

  validates_presence_of :name
end
